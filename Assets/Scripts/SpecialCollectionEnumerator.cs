﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SpecialCollectionEnumerator : IEnumerator
{
    private SpecialCollection collection;
    private int Pos = -1;
    public SpecialCollectionEnumerator(SpecialCollection sc)
    {
        collection = sc;
    }

    public object Current
    {
        get
        {
            return collection.GetArr.OrderBy(c => c.Length).ToArray()[Pos];
        }
    }

    public bool MoveNext()
    {
        if(Pos < collection.GetArr.Length)
        {
            Pos++;
        }
        return Pos < collection.GetArr.Length;
    }

    public void Reset()
    {
        Pos = -1;
    }
}