﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IteratorTest : MonoBehaviour
{
    public void Start()
    {
        SpecialCollection newCol = new SpecialCollection(new string[] { "один", "два", "три", "четыре", "пять" },new float[] { 0.345f, 0.2f, 22f, 1f, 56.663f });
        foreach (string s in newCol)
        {
            Debug.Log(s);
        }
    }
}