﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialCollection : IEnumerable
{
    public float[] arrfloat;
    public string[] arrstr;
    public string[] GetArr
    {
        get
        {
            List<string> a = new List<string>(arrstr);
            foreach (float b in arrfloat)
            {
                a.Add(b.ToString());
            }
            return a.ToArray();
        }
    }
    
    public SpecialCollection(string[] arr,float[] arr2)
    {
        arrstr = arr;
        arrfloat = arr2;
    }
    public IEnumerator GetEnumerator()
    {
        return new SpecialCollectionEnumerator(this);
    }
}